const postsXHR = new XMLHttpRequest();
postsXHR.open('GET', 'https://jsonplaceholder.typicode.com/posts');
postsXHR.responseType = 'json';
postsXHR.onload = () => {
    const posts = postsXHR.response;
    const usersXHR = new XMLHttpRequest();
    usersXHR.open('GET', 'https://jsonplaceholder.typicode.com/users');
    usersXHR.responseType = 'json';
    usersXHR.onload = () => {
        const users = usersXHR.response;
        posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            const postProps = {
                id: post.id,
                name: user.name,
                email: user.email,
                title: post.title,
                text: post.body,
                userID: user.id
            };

            const card = new Card(postProps)
        })
    };
    usersXHR.send()
};

postsXHR.send();


class Card {
    constructor({id, name, email, title, text, userID}) {
        this.container = document.querySelector('.container');
        this.item = document.createElement('div');
        this.item.className = 'card';
        this.item.id = id;
        this.id = id;
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.userID = userID;
        this.url = 'https://jsonplaceholder.typicode.com/posts';
        this.renderCard(name, email, title, text, id);
        this.bindedClickHandler = this.saveChangesHandler.bind(this);
        this.addListeners(id);
    }

    addListeners(id) {
        this.item.querySelector('#close-btn').addEventListener('click',() => {
            this.removeCard(id)
        });

        this.item.querySelector('#change-btn').addEventListener('click',() => {
            document.querySelector('#save-changes').addEventListener('click', this.bindedClickHandler)
        })
    }

    renderCard(name, email, title, text) {
        this.item.innerHTML = `
            <div class="card-buttons">
              <button class="card-buttons__button" data-toggle="modal" data-target="#edit-modal" id="change-btn">
                <i class="fa fa-pencil-square-o"></i>
              </button>
              <button class="card-buttons__button" id="close-btn">
                <i class="fa fa-times"></i>
              </button>
            </div>
            <div class="card-body">
              <img alt="Avatar"
                   src="https://i7.pngguru.com/preview/178/419/741/computer-icons-avatar-login-user-avatar.jpg"
                   class="user-avatar">
              <div class="card-content">
                <h3 class="user-name">
                  ${name}
                </h3>
                <span class="user-email">
                    ${email}
                </span>
                <div class="user-heading">
                  ${title}
                </div>
                <p class="user-text">
                  ${text}
                </p>
              </div>
            </div>`;

        this.container.append(this.item);
    }

    saveChangesHandler(e) {
        const changeProps = this.getChanges();
        this.editCard(this.id, changeProps);

        e.target.removeEventListener('click', this.bindedClickHandler)
    }

    removeCard(id) {
        this.item.remove();

        const xhr = new XMLHttpRequest();
        xhr.open('DELETE', `${this.url}/${id}`);
        xhr.send();
    }


    editCard(id, {title, text}) {
        const cardTitle = this.item.querySelector('.user-heading');
        const cardText = this.item.querySelector('.user-text');

        cardTitle.innerText = title;
        cardText.innerText = text;

        const xhr = new XMLHttpRequest();
        xhr.open('PUT', `${this.url}/${id}`);
        const data = JSON.stringify({
            id: id,
            title: title,
            body: text,
            userId: this.userID
        });
        xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        xhr.send(data);
    }

    getChanges () {
        const titleField = document.getElementById('title-field');
        const textField = document.getElementById('text-field');

        return {
            title: titleField.value || this.title,
            text: textField.value || this.text
        }
    }
}



//
// async function getData(type) {
//     let res = await function () {
//         return new Promise(((resolve) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open('GET', `https://jsonplaceholder.typicode.com/${type}`);
//             xhr.responseType = 'json';
//             xhr.onload = () => {
//                 console.log(xhr.response);
//                 resolve(xhr.response)
//             };
//             xhr.send();
//         }));
//
//     }();
//     console.log(res);
// }