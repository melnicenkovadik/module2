const container = document.querySelector('.container');

function sendRequest() {
    const xhr = new XMLHttpRequest();
    const url = 'https://swapi.co/api/films/';

    xhr.open('GET', url);
    xhr.responseType = 'json';
    xhr.send();
    xhr.onload = () => {
        xhr.response.results.forEach((movie, index) => {
            const movieCard = document.createElement('div');
            movieCard.id = index;

            const name = document.createElement('h3');
            const id = document.createElement('span');
            const crawls = document.createElement('p');
            const loader = document.createElement('div');
            loader.innerHTML = `<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;

            name.innerText = movie.title;
            id.innerText = movie.episode_id;
            crawls.innerText = movie.opening_crawl;

            movieCard.append(name, id, crawls, loader)
            container.append(movieCard);

            const chars = document.createElement('ul');
            const characters = movie.characters;
            let count = 0;
            characters.forEach(el => getCharacters(el, cb, characters.length));

            function cb(res, n) {
                count++;
                const li = document.createElement('li');
                li.innerText = res.name;
                chars.append(li)
                if (count >= n) {
                    const currentMovie = document.getElementById(index);
                    currentMovie.append(chars)
                    currentMovie.querySelector('.lds-ring').remove()
                }
            }

            function getCharacters(url, cb, length) {
                const xhr = new XMLHttpRequest();
                xhr.open('GET', url);
                xhr.responseType = 'json';
                xhr.send();
                xhr.onload = () => {
                    cb(xhr.response, length)
                }
            }

        });
    }

}

sendRequest();

