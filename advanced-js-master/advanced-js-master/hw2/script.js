class HamburgerException extends Error {
    constructor(message, param) {
        super();
        this.name = 'HamburgerException';
        this.message = message;
        this.param = param;
    }
}

class Hamburger {
     static hamburgerProps = {
        SIZE_SMALL: {type: 'size', param: 'SIZE_SMALL', price: 50, calories: 20},
        SIZE_LARGE: {type: 'size', param: 'SIZE_LARGE', price: 100, calories: 40},
        STUFFING_CHEESE: {type: 'stuffing', param: 'STUFFING_CHEESE', price: 10, calories: 40},
        STUFFING_SALAD: {type: 'stuffing', param: 'STUFFING_SALAD', price: 20, calories: 40},
        STUFFING_POTATO: {type: 'stuffing', param: 'STUFFING_POTATO', price: 15, calories: 40},
        TOPPING_MAYO: {type: 'topping', param: 'TOPPING_MAYO', price: 15, calories: 0},
        TOPPING_SPICE: {type: 'topping', param: 'TOPPING_SPICE', price: 20, calories: 5}
    };

    constructor(size, stuffing) {
        try {
            if (!size) {
                throw new HamburgerException('no size given')
            }
            if (!stuffing) {
                throw new HamburgerException('no stuffing given')
            }
            // const {hamburgerProps} = this;
            if (!this.constructor.hamburgerProps[size] || this.constructor.hamburgerProps[size].type !== 'size') {
                throw new HamburgerException('invalid size', size)
            }
            if (!this.constructor.hamburgerProps[stuffing] || this.constructor.hamburgerProps[stuffing].type !== 'stuffing') {
                throw new HamburgerException('invalid stuffing', stuffing)
            }

            this.size = size;
            this.stuffing = stuffing;
            this.topping = [];

        } catch (err) {
            console.error(`${err.name}: ${err.message} '${err.param || ''}'`)
        }
    }

    set addTopping(topping) {
        try {
            if (!this.constructor.hamburgerProps[topping] || this.constructor.hamburgerProps[topping].type !== 'topping') {
                throw new HamburgerException('invalid topping', topping)
            }
            if (this.topping.indexOf(topping) !== -1) {
                throw new HamburgerException('duplicate topping', topping)
            }

            this.topping.push(topping)
        } catch (err) {
            console.error(`${err.name}: ${err.message} '${err.param || ''}'`)
        }
    }

    set removeTopping(topping) {
        try {
            if (this.topping.indexOf(topping) === -1) {
                throw new HamburgerException('no such topping', topping)
            }

            for (let i = 0; i < this.topping.length; i++) {
                if (this.topping[i] === topping) {
                    this.topping.splice(i, 1)
                }
            }

        } catch (err) {
            console.error(`${err.name}: ${err.message} '${err.param || ''}'`)
        }
    }

    get getToppings () {
        return this.topping
    };

    get getSize () {
        return this.size
    };

    get getStuffing () {
        return this.stuffing
    };

    get price() {
        const size = this.size;
        const stuffing = this.stuffing;
        const topping = this.topping;
        let price = this.constructor.hamburgerProps[size].price + this.constructor.hamburgerProps[stuffing].price;

        topping.forEach(topping => {
            price += this.constructor.hamburgerProps[topping].price
        });

        return price
    }

    get calories() {
        const size = this.size;
        const stuffing = this.stuffing;
        const topping = this.topping;
        let calories = this.constructor.hamburgerProps[size].calories + this.constructor.hamburgerProps[stuffing].calories;

        topping.forEach(topping => {
            calories += this.constructor.hamburgerProps[topping].calories
        });

        return calories
    }
}

const hamburger = new Hamburger('SIZE_SMALL', 'STUFFING_SALAD');

console.log(hamburger);
hamburger.addTopping = 'TOPPING_MAYO';
hamburger.addTopping = 'TOPPING_SPICE';
hamburger.removeTopping = 'TOPPING_MAYO';
console.log(hamburger.getToppings);
console.log(hamburger.getStuffing);
console.log(hamburger.getSize);
console.log(hamburger.price);
console.log(hamburger.calories);

console.log(hamburger);


