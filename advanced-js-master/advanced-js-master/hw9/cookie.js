const btn = document.getElementById('contact-btn');

btn.addEventListener('click', () => {
    setCookie('experiment', 'no-value', 300);
    validateUser('new-user');

    console.log(getCookie('new-user'));
});

function setCookie(name, value, age) {
    document.cookie = `${encodeURIComponent(name)}=${encodeURIComponent(value)}; ${age ? `max-age=${age}` : ''}`
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function validateUser(cookieName) {
    const name = cookieName;

    if (getCookie(name) === undefined) {
        setCookie(name, 'true');
    } else if (getCookie(name)) {
        setCookie(name, 'false')
    } else {
        return
    }
}


