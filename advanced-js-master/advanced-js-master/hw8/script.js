const btn = document.getElementById('ip-button');

btn.addEventListener('click', async () => {
    const {ip} = await getIP();
    const data = await getData(ip);

    showData(data)

});

async function getIP() {
    const response = await fetch('https://api.ipify.org/?format=json');
    const IP = await response.json();

    return IP
}

async function getData(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`);
    const data = await response.json();

    return data
}

function showData(data){
    console.log(...arguments);
    const dataList = document.getElementById('data-list');


    Object.entries(data).map(([key, value]) => {
        const dataItem = document.createElement('li');
        dataItem.classList.add('list-item');

        dataItem.innerText = `${key}: ${value}`;
        dataList.append(dataItem)
    })


}

