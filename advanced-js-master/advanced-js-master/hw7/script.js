const container = document.querySelector('.container');
const moviesUrl = 'https://swapi.co/api/films/';

fetch(moviesUrl)
    .then(response => response.json())
    .then(response => {
        const result = response.results;
        console.log(result);
        result.forEach((movie, index) => {
            renderMovies(movie, index);

            const chars = movie.characters.map(char => fetch(char))
            Promise.all(chars)
                .then(response => Promise.all(response.map(res => res.json())))
                .then(response => renderChars(response, index))
        });
    });

function renderMovies(movie, index) {
    const movieCard = document.createElement('div');
    movieCard.id = index;

    const name = document.createElement('h3');
    const id = document.createElement('span');
    const crawls = document.createElement('p');
    const loader = document.createElement('div');
    loader.innerHTML = `<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;

    name.innerText = movie.title;
    id.innerText = movie.episode_id;
    crawls.innerText = movie.opening_crawl;

    movieCard.append(name, id, crawls, loader)
    container.append(movieCard);
}

function renderChars(charsArr, index) {
    const currentMovie = document.getElementById(index);
    const charsList = document.createElement('ul')

    charsArr.map(char => {
        const item = document.createElement('li');
        item.innerText = char.name
        charsList.append(item)
    });
    currentMovie.querySelector('.lds-ring').remove()
    currentMovie.append(charsList)
}

