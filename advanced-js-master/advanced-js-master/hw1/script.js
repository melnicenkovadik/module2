function HamburgerException(message, param) {
    this.name = 'HamburgerException';
    this.message = message;
    this.param = param;
}

HamburgerException.prototype = Object.create(TypeError.prototype)


function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException('no size given')
        }
        if (!stuffing) {
            throw new HamburgerException('no stuffing given')
        }
        if (!this.constructor[size] || this.constructor[size].type !== 'size' ) {
            throw new HamburgerException('invalid size', size)
        }
        if (!this.constructor[stuffing] || this.constructor[stuffing].type !== 'stuffing' ) {
            throw new HamburgerException('invalid stuffing', stuffing)
        }

        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];

    } catch (err) {
        console.error(err.name + ': ' + err.message + ' \'' + (err.param || '') + '\'');
    }
}

Hamburger.SIZE_SMALL = {type: 'size', param: 'SIZE_SMALL', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {type: 'size', param: 'SIZE_LARGE', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {type: 'stuffing', param: 'STUFFING_CHEESE', price: 10, calories: 40};
Hamburger.STUFFING_SALAD = {type: 'stuffing', param: 'STUFFING_SALAD', price: 20, calories: 40};
Hamburger.STUFFING_POTATO = {type: 'stuffing', param: 'STUFFING_POTATO', price: 15, calories: 40};
Hamburger.TOPPING_MAYO = {type: 'topping', param: 'TOPPING_MAYO', price: 15, calories: 0};
Hamburger.TOPPING_SPICE = {type: 'topping', param: 'TOPPING_SPICE', price: 20, calories: 5};

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!this.constructor[topping] || this.constructor[topping].type !== 'topping') {
            throw new HamburgerException('invalid topping', topping)
        }
        if (this.topping.indexOf(topping) !== -1) {
            throw new HamburgerException('duplicate topping', topping)
        }

        this.topping.push(topping)
    } catch (err) {
        console.error(err.name + ': ' + err.message + ' \'' + (err.param || '') + '\'');
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.topping.indexOf(topping) === -1) {
            throw new HamburgerException('no such topping', topping)
        }

        for (let i = 0; i < this.topping.length; i++) {
            if (this.topping[i] === topping) {
                this.topping.splice(i, 1)
            }
        }

    } catch (err) {
        console.error(err.name + ': ' + err.message + ' \'' + (err.param || '') + '\'');
    }
};

Hamburger.prototype.getToppings = function () {
    return this.topping
};

Hamburger.prototype.getSize = function () {
    return this.size
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

Hamburger.prototype.calculatePrice = function () {
    var size = this.size;
    var stuffing = this.stuffing;
    var topping = this.topping;
    var price = this.constructor[size].price + this.constructor[stuffing].price;

    topping.forEach(topping => {
        price += this.constructor[topping].price
    });

    return price
};

Hamburger.prototype.calculateCalories = function () {
    var size = this.size;
    var stuffing = this.stuffing;
    var topping = this.topping;
    var calories = this.constructor[size].calories + this.constructor[stuffing].calories;

    topping.forEach(topping => {
        calories += this.constructor[topping].calories
    });

    return calories
};

var hamburger = new Hamburger('SIZE_SMALL', 'STUFFING_SALAD');
hamburger.addTopping('TOPPING_SPICE');
hamburger.addTopping('TOPPING_MAYO');
hamburger.removeTopping('TOPPING_SPICE');
console.log(hamburger.getSize());
console.log(hamburger.getStuffing());
console.log(hamburger.getToppings());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());


console.log(hamburger);
