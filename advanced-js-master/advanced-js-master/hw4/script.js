class Card {
    constructor(name, colName) {
        this.name = name;
        this.parent = document.getElementById(`cards-list-${colName}`);
        this.card = document.createElement('div');
        this.card.classList.add('cards-item');
        this.card.setAttribute('draggable', 'true');
        this.card.innerText = name;
        this.card.id = `card-item-${name}`;
        this.addDragAndDrop(this.card)
    }

    renderCard(card, container) {
        container.append(card)
    }

    addDragAndDrop(card) {
        card.addEventListener('dragstart', (e) => {
            this.handleDragStart(e, this)
        }, false);
        card.addEventListener('dragenter', this.handleDragEnter, false);
        card.addEventListener('dragover', this.handleDragOver, false);
        card.addEventListener('dragleave', this.handleDragLeave, false);
        card.addEventListener('drop', (e) => {
            this.handleDrop(e)
        }, false);
        card.addEventListener('dragend', this.handleDragEnd, false);
    }


    handleDragStart(e, card) {
        e.target.style.opacity = '0.4';
        e.target.style.opacity = '0.4';

        e.dataTransfer.effectAllowed = 'move';

        e.dataTransfer.setData('text/html', e.target.innerHTML);
    }

    handleDragOver(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        e.dataTransfer.dropEffect = 'move';
    }

    handleDragEnter(e) {
        e.target.classList.add('over');
    }

    handleDragLeave(e) {
        e.target.classList.remove('over');
    }

    handleDrop(e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }

        const source = document.getElementById(`card-item-${e.dataTransfer.getData('text/html')}`);

        source.id = `card-item-${e.target.innerHTML}`;
        e.target.id = `card-item-${e.dataTransfer.getData('text/html')}`;

        source.innerHTML = e.target.innerHTML;
        e.target.innerHTML = e.dataTransfer.getData('text/html');
    }

    handleDragEnd(e) {
        e.target.style.opacity = '1';
        document.querySelector('.over').classList.remove('over')
    }
}

class Column {
    constructor(name) {
        this.name = name;
        this.cards = [];
        this.card = new Card();
        this.renderColumn();
        this.container = document.getElementById(`cards-list-${this.name}`)
    }

    renderColumn() {
        const columnContainer = document.getElementById('columns');
        const column = document.createElement('div');
        const columnHeader = document.createElement('div');
        const columnHeading = document.createElement('h2');
        const sortButton = document.createElement('button');
        const cardList = document.createElement('div');
        const addCardForm = document.createElement('form');

        column.classList.add('app-column');
        columnHeader.classList.add('column-header');
        columnHeading.classList.add('column-heading');
        columnHeading.innerText = this.name;
        sortButton.classList.add('btn-sort');
        sortButton.innerText = 'Sort';
        sortButton.id = `sort-button-${this.name}`;
        cardList.classList.add('cards-list');
        cardList.id = `cards-list-${this.name}`;
        addCardForm.classList.add('add-card');

        columnHeader.append(columnHeading, sortButton);
        column.append(columnHeader, cardList, addCardForm);

        addCardForm.innerHTML = `<input id="card-name-${this.name}" type="text" class="column-input" placeholder="Card name">
                                    <button id="add-card-btn-${this.name}" class="btn btn-submit">
                                        <i class="fa fa-plus"></i>
                                    </button>`;

        columnContainer.append(column);
        this.addListeners()
    }

    addListeners() {
        document.getElementById(`sort-button-${this.name}`).addEventListener('click', () => {
            this.sortCards();
        });

        document.getElementById(`add-card-btn-${this.name}`).addEventListener('click', () => {
            this.addCard();
        });

    }

    addCard() {
        const nameInput = document.getElementById(`card-name-${this.name}`);
        const cardName = nameInput.value;
        if (cardName) {
            const newCard = new Card(cardName, this.name);
            this.cards.push(newCard);
            this.card.renderCard(newCard.card, this.container);
            nameInput.value = '';
        }
    }

    renderCards() {
        this.container.innerHTML = '';

        this.cards.forEach(item => {
            this.card.renderCard(item.card, this.container)
        })
    }

    sortCards() {
        this.cards.sort((a, b) => a.card.innerHTML > b.card.innerHTML ? 1 : -1)
        this.renderCards()
    }
}

const columns = {};

const createColumn = document.getElementById('column-add');
createColumn.addEventListener('click', () => {
    const name = document.getElementById('column-name');
    if (columns[[`column${name.value}`]]) {
        alert('Please enter unique name!')
    } else columns[`column${name.value}`] = new Column(name.value);
    name.value = '';
});