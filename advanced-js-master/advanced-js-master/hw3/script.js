class Game {
    constructor() {
        this.field = document.getElementById('game');
        this.level = null;
        this.timer = null;
        this.userScore = 0;
        this.AIScore = 0;
        this.startBtn = document.getElementById('start-btn');
        this.levelSelect = document.getElementById('level')
        this.startBtn.addEventListener('click', this.settingsHandler.bind(this));
        this.bindedClickHandler = this.userClickHandler.bind(this);
    }

    static get STYLES() {
        return {
            'active': 'active',
            'user': 'user',
            'AI': 'ai'
        }
    };

    static get LEVELS() {
        return {
            easy: 1500,
            medium: 1000,
            hard: 500
        }
    };

    settingsHandler() {
        const chosenLevel = document.getElementById('level').value;
        this.level = chosenLevel;
        this.startBtn.setAttribute('disabled', 'true');
        this.levelSelect.setAttribute('disabled', 'true');
        this.startGame(this.level)
    }

    startGame(level) {
        const currentHole = this.getRandomHole();
        console.log(currentHole);

        if (currentHole) {
            currentHole.classList.add(Game.STYLES['active']);
            this.timer = setTimeout(this.AIHandler.bind(this, currentHole, level), Game.LEVELS[level]);
            currentHole.addEventListener('click', this.bindedClickHandler)
        } else {
            this.finishGame()
        }
    }

    userClickHandler(e) {
        this.userScore++;
        e.target.classList.add(Game.STYLES['user']);
        e.target.removeEventListener('click', this.bindedClickHandler);
        clearTimeout(this.timer);
        this.startGame(this.level)
    }

    AIHandler(currentHole, level) {
        currentHole.classList.remove(Game.STYLES['active']);
        currentHole.classList.add(Game.STYLES['AI']);
        currentHole.removeEventListener('click', this.bindedClickHandler);

        this.AIScore++
        this.startGame(level)
    }

    getRandomHole() {
        const freeHoles = document.querySelectorAll('.mole:not(.ai):not(.user)');
        const randomIndex = Math.floor(Math.random() * freeHoles.length);
        return freeHoles[randomIndex]
    }

    finishGame() {
        if (this.AIScore > this.userScore) {
            alert('You Lose!')
        } else if (this.AIScore < this.userScore) {
            alert('You Win!')
        } else {
            alert('Draw!')
        }
        this.startBtn.removeAttribute('disabled');
        [...this.field.querySelectorAll('.mole')].forEach(item => {
            item.classList.remove('ai', 'user');
        });

        this.levelSelect.removeAttribute('disabled');
        this.startBtn.removeAttribute('disabled')
    }
}

const whackAMole = new Game();