### План урока

1. Доверстать макет с прошлого занятия.
2. Сверстать интерактивные секции макета theHAM, используя встроенные в Bootstrap JS-компоненты.
3. Разделится по командам.
4. Step Project 2 - ответы на вопросы.
4. Начать верстать Step Project 2 в командах.

#### Ссылки, которые вам могут пригодится

[всплывающее окно](https://www.w3schools.com/bootstrap4/bootstrap_ref_js_modal.asp)
[Показать/скрыть при клике](https://www.w3schools.com/bootstrap4/bootstrap_ref_js_collapse.asp)
[карусель/слайдер](https://www.w3schools.com/bootstrap4/bootstrap_ref_js_carousel.asp)
[Tabs (переключаемые по клику вкладки)](https://www.w3schools.com/bootstrap4/bootstrap_ref_js_tab.asp)
[Фильтр товаров](https://bootsnipp.com/snippets/1dPDV)
