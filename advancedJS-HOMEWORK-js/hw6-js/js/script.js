$.ajax({
  url: 'https://swapi.co/api/people/',
  success: function (data) {
    console.log(data);
    const resultsArr = data.results;
    console.log(resultsArr);

    const div = document.getElementById('episodes');

    resultsArr.forEach(function (element) {
      $.get(element.homeworld, function (data) {

        const container = document.createElement('div');
        container.innerHTML = `
 <h1 class="episode-title">${element.name}</h1>
 <p class="">Gender: ${element.gender}</p>
 <p class="crawl">World: ${data.name}</p>
 <div class="episode-people">
 </div>
`;
        container.classList.add('episode-container');
        const starShips = element.starships;

        if (element.starships.length > 0) {
          const button = document.createElement('button');
          button.innerText = 'Список кораблей';
          button.classList.add('button');
          container.append(button);
          button.addEventListener('click', function (event) {
            event.preventDefault();
            button.remove();

            const starShipsContainer = document.createElement('div');
            container.append(starShipsContainer);
            starShips.forEach(function (item, index) {
              $.ajax({
                url: item,
                async: false,
                success: function (data) {
                  if (index === starShips.length - 1) {
                    const starShipName = data.name;
                    const span = document.createElement('span');
                    span.innerText = ` ${starShipName}.`;
                    span.classList.add('starships');
                    starShipsContainer.append(span);
                  } else {
                    if (index === 0) {
                      const starShipName = data.name;
                      const span = document.createElement('span');
                      span.innerText = `Пилотируемые корабли внутри него: ${starShipName}, `;
                      span.classList.add('starships');
                      starShipsContainer.append(span);
                    } else {
                      const starShipName = data.name;
                      const span = document.createElement('span');
                      span.innerText = `${starShipName}, `;
                      span.classList.add('starships');
                      starShipsContainer.append(span);
                    }
                  }
                },
              })
            })
          })
        }
        div.append(container);
      });
    });
  },
  error: function () {
    const div = document.getElementById('episodes');
    div.innerHTML = `<h1 class="error-text">Что-то пошло не так, ведутся тех работы, но это не точно =)</h1>`
  }
});
