class Animal {
    constructor(name) {
        this.name = name;
    }
}

class Rabbit {
    jump() {
        console.log(this.name + " jumps.");
    }
}

let theBrave = new Rabbit("The Brave");
theBrave.jump();