### Информация, которая пригодится вам по ходу урока

#### Добавить Bootstrap 4 на страницу

`<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">`

#### Различия между 3 и 4 версиями Bootstrap

https://www.quackit.com/bootstrap/bootstrap_4/differences_between_bootstrap_3_and_bootstrap_4.cfm

#### Пример прототипа сайта
https://drive.google.com/open?id=1rR6CDBBgMd-qcHEPA2xMKvBQYeJz95Re

#### Создать свою версию Boostrap 4, слегка поменяв настройки
http://upgrade-bootstrap.bootply.com/bootstrap-4-customizer

#### Создать свою версию Boostrap 4, сильно изменив настройки
https://themestr.app/customize

#### Где можно взять бесплатные HTML шаблоны на Boostrap
https://mobirise.com/bootstrap-4-theme/

#### Множество готовых модулей
https://bootsnipp.com

#### Где подсмотреть готовые решения компонентов

Сетка: https://www.w3schools.com/bootstrap4/bootstrap_grid_system.asp 

Верхнее меню навигации - https://www.w3schools.com/bootstrap4/bootstrap_navbar.asp 

Кнопки: https://www.w3schools.com/bootstrap4/bootstrap_buttons.asp 

Форма: https://www.w3schools.com/bootstrap4/bootstrap_forms.asp

Поля ввода: https://www.w3schools.com/bootstrap4/bootstrap_forms_inputs.asp 





